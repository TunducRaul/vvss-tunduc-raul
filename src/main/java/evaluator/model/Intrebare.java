package evaluator.model;

import java.util.Objects;

public class Intrebare {

    private String enunt;
    private String varianta1;
    private String varianta2;
    private String variantaCorecta;
    private String domeniu;

    public Intrebare() {
    }

    public Intrebare(String enunt, String varianta1, String varianta2, String variantaCorecta, String domeniu) {
        this.enunt = enunt;
        this.varianta1 = varianta1;
        this.varianta2 = varianta2;
        this.variantaCorecta = variantaCorecta;
        this.domeniu = domeniu;
    }

    public String getEnunt() {
        return enunt;
    }

    public void setEnunt(String enunt) {
        this.enunt = enunt;
    }

    public String getVarianta1() {
        return varianta1;
    }

    public void setVarianta1(String varianta1) {
        this.varianta1 = varianta1;
    }

    public String getVarianta2() {
        return varianta2;
    }

    public void setVarianta2(String varianta2) {
        this.varianta2 = varianta2;
    }

    public String getVariantaCorecta() {
        return variantaCorecta;
    }

    public void setVariantaCorecta(String variantaCorecta) {
        this.variantaCorecta = variantaCorecta;
    }

    public String getDomeniu() {
        return domeniu;
    }

    public void setDomeniu(String domeniu) {
        this.domeniu = domeniu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Intrebare intrebare = (Intrebare) o;
        return Objects.equals(enunt, intrebare.enunt) &&
                Objects.equals(varianta1, intrebare.varianta1) &&
                Objects.equals(varianta2, intrebare.varianta2) &&
                Objects.equals(variantaCorecta, intrebare.variantaCorecta) &&
                Objects.equals(domeniu, intrebare.domeniu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enunt, varianta1, varianta2, variantaCorecta, domeniu);
    }

    @Override
    public String toString() {
        return "Intrebare{" +
                "enunt='" + enunt + '\'' +
                ", varianta1='" + varianta1 + '\'' +
                ", varianta2='" + varianta2 + '\'' +
                ", variantaCorecta='" + variantaCorecta + '\'' +
                ", domeniu='" + domeniu + '\'' +
                '}';
    }
}