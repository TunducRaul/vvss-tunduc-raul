package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.ExceptionMessages;
import evaluator.exception.InputValidationFailedException;
import org.junit.Test;

public class Test_ECP {

    private AppController appController = new AppController("noData.txt");

    @Test
    public void TC2_ECP() {
        try {
            appController.addNewIntrebare("Ce ai facut azi?", "1) Nimic", "2) De toate", "1", "Random");
            assert true;
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    public void TC12_ECP() {
        try {
            appController.addNewIntrebare("", "1) Nimic", "2) De toate", "1", "Random");
            assert false;
        } catch (InputValidationFailedException | DuplicateIntrebareException ex) {
            assert ex.getMessage().equals(ExceptionMessages.ENUNT_VID);
        }
    }

    @Test
    public void TC3_ECP() {
        try {
            appController.addNewIntrebare("ce ai facut azi?", "1) Nimic", "2) De toate", "1", "Random");
            assert false;
        } catch (InputValidationFailedException | DuplicateIntrebareException ex) {
            assert ex.getMessage().equals(ExceptionMessages.PRIMA_LITERA_ENUNT);
        }
    }

    @Test
    public void TC13_ECP() {
        try {
            appController.addNewIntrebare("Ce ai facut azi?", "", "2) De toate", "1", "Random");
            assert false;
        } catch (InputValidationFailedException | DuplicateIntrebareException ex) {
            assert ex.getMessage().equals(ExceptionMessages.PRIMA_VARIANTA_VIDA);
        }
    }

    @Test
    public void TC14_ECP() {
        try {
            appController.addNewIntrebare("Ce ai facut azi?", "1) Nimic", "", "1", "Random");
            assert false;
        } catch (InputValidationFailedException | DuplicateIntrebareException ex) {
            assert ex.getMessage().equals(ExceptionMessages.A_DOUA_VARIANTA_VIDA);
        }
    }

    @Test
    public void TC15_ECP() {
        try {
            appController.addNewIntrebare("Ce ai facut azi?", "1) Nimic", "2) De toate", "", "Random");
            assert false;
        } catch (InputValidationFailedException | DuplicateIntrebareException ex) {
            assert ex.getMessage().equals(ExceptionMessages.VARIANTA_INCORECTA);
        }
    }
}