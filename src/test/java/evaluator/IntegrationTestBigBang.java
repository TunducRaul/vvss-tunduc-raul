package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(IntegrationTest.class)
public class IntegrationTestBigBang {
    private static String DOMENIU_VALID = "Intrebare1";

    private IntrebariRepository repo;
    private AppController appController;

    @Before
    public void setData() {
        repo = new IntrebariRepository();
        appController = new AppController("DateCorecte.txt");
    }

    @Test
    public void test_A() throws DuplicateIntrebareException {
        /*
         * Test A
         */
        test_BVA();
    }

    @Test
    public void test_B() {
        /*
         * Test B
         */
        test_path_three_four_five();
    }

    @Test
    public void test_C() {
        /*
         * Test C
         */
        test_valid_data();
    }

    @Test
    public void test_PABC() throws DuplicateIntrebareException {
        test_BVA();
        test_path_three_four_five();
        test_valid_data();
    }

    private void test_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("V?", "1) Foarte Multe", "2) Nici una", "1", "Matematica");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }

    private void test_path_three_four_five() {
        try {
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void test_valid_data() {
        try {
            Statistica statistica = appController.getStatistica();
            assert statistica.getNumarIntrebari(DOMENIU_VALID).equals(1);
        } catch (Exception ex) {
            assert false;
        }
    }
}