package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.ExceptionMessages;
import org.junit.Test;

public class Test_WBT {

    @Test
    public void test_path_one() {
        try {
            AppController appController = new AppController("NumarIntrebariInvalid.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals(ExceptionMessages.NU_EXISTA_SUFICIENTE_INTREBARI));
        }
    }

    @Test
    public void test_path_two() {
        try {
            AppController appController = new AppController("NumarDomeniiInvalid.txt");
            evaluator.model.Test test = appController.createNewTest();
        } catch (Exception ex) {
            assert (ex.getMessage().equals(ExceptionMessages.NU_EXISTA_SUFICIENTE_DOMENII));
        }
    }

    @Test
    public void test_path_three_four_five() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            evaluator.model.Test test = appController.createNewTest();
            assert (test.getIntrebari().size() == 5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}