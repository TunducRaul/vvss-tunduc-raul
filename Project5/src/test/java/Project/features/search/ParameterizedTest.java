package Project.features.search;

import Project.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/TestData.csv")
public class ParameterizedTest {

    @Managed(uniqueSession = true)
    public WebDriver webDriver;

    @ManagedPages(defaultUrl = "file:///E:/vvss-tunduc-raul/index.html")
    public Pages pages;

    public String enunt;
    public String varianta1;
    public String varianta2;
    public String variantacorecta;
    public String domeniu;
    public String definition;

    @Qualifier
    public String getEnunt() {
        return enunt;
    }

    @Qualifier
    public String getVarianta1() {
        return varianta1;
    }

    @Qualifier
    public String getVarianta2() {
        return varianta2;
    }

    @Qualifier
    public String getVariantaCorecta() {
        return variantacorecta;
    }

    @Qualifier
    public String getDomeniu() {
        return domeniu;
    }

    @Qualifier
    public String getDefinition() {
        return definition;
    }

    @Qualifier
    public void setEnunt(String enunt) {
        this.enunt = enunt;
    }

    @Qualifier
    public void setVarianta1(String varianta1) {
        this.varianta1 = varianta1;
    }

    @Qualifier
    public void setVarianta2(String varianta2) {
        this.varianta2 = varianta2;
    }

    @Qualifier
    public void setVariantaCorecta(String variantacorecta) {
        this.variantacorecta = variantacorecta;
    }

    @Qualifier
    public void setDomeniu(String domeniu) {
        this.domeniu = domeniu;
    }

    @Qualifier
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Steps
    public EndUserSteps endUserSteps;

    @Issue("#WIKI-1")
    @Test
    public void testData() {
        endUserSteps.is_the_home_page();
        endUserSteps.looks_for(getEnunt(), getVarianta1(), getVarianta2(), getVariantaCorecta(), getDomeniu());
        endUserSteps.should_see_definition(getDefinition());
    }
}