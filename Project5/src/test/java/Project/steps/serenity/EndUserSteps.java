package Project.steps.serenity;

import Project.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getDefinition(), hasItem(containsString(definition)));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

    @Step
    public void looks_for(String enunt, String varianta1, String varianta2, String variantaCorecta, String domeniu) {
        dictionaryPage.enterEnunt(enunt);
        dictionaryPage.enterVarianta1(varianta1);
        dictionaryPage.enterVarianta2(varianta2);
        dictionaryPage.enterVariantaCorecta(variantaCorecta);
        dictionaryPage.enterDomeniu(domeniu);
        dictionaryPage.lookup_terms();
    }
}