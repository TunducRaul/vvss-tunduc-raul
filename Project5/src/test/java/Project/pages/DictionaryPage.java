package Project.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.List;

@DefaultUrl("file:///E:/vvss-tunduc-raul/index.html")
public class DictionaryPage extends PageObject {

    @FindBy(id="enunt")
    private WebElementFacade enunt;

    @FindBy(id="varianta1")
    private WebElementFacade varianta1;

    @FindBy(id="varianta2")
    private WebElementFacade varianta2;

    @FindBy(id="variantaCorecta")
    private WebElementFacade variantaCorecta;

    @FindBy(id="domeniu")
    private WebElementFacade domeniu;

    @FindBy(id="btnAddIntrebare")
    private WebElementFacade lookupButton;

    public void enterEnunt(String keyword) {
        enunt.type(keyword);
    }

    public void enterVarianta1(String keyword) {
        varianta1.type(keyword);
    }

    public void enterVarianta2(String keyword) {
        varianta2.type(keyword);
    }

    public void enterVariantaCorecta(String keyword) {
        variantaCorecta.type(keyword);
    }

    public void enterDomeniu(String keyword) {
        domeniu.type(keyword);
    }

    public void lookup_terms() {
        lookupButton.click();
    }

    public List<String> getDefinition() {
        return Collections.singletonList(find(By.id("result")).getText());
    }
}