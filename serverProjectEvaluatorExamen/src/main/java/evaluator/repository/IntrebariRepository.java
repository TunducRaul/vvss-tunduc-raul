package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.ExceptionMessages;
import evaluator.model.Intrebare;
import evaluator.util.InputValidation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class IntrebariRepository {
    private List<Intrebare> intrebari;

    public IntrebariRepository() {
        intrebari = new LinkedList<>();
    }

    public String addIntrebare(Intrebare i) throws DuplicateIntrebareException {
        if (exists(i))
            throw new DuplicateIntrebareException(ExceptionMessages.INTREBARE_EXISTENTA);
        intrebari.add(i);
        return "quiz added";
    }

    public boolean exists(Intrebare i) {
        Iterator<Intrebare> iterator = intrebari.iterator();
        while (iterator.hasNext()) {
            Intrebare intrebare = iterator.next();
            if (intrebare.equals(i)) {
                return true;
            }
        }
        return false;
    }

    public Intrebare pickRandomIntrebare() {
        Random random = new Random();
        return intrebari.get(random.nextInt(intrebari.size()));
    }

    public int getNumberOfDistinctDomains() {
        return getDistinctDomains().size();
    }

    public Set<String> getDistinctDomains() {
        Set<String> domains = new TreeSet<String>();
        for (Intrebare intrebre : intrebari)
            domains.add(intrebre.getDomeniu());
        return domains;
    }

    public List<Intrebare> getIntrebariByDomain(String domain) {
        List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
        for (Intrebare intrebare : intrebari) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebariByDomain.add(intrebare);
            }
        }
        return intrebariByDomain;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebariByDomain(domain).size();
    }

    public List<Intrebare> loadIntrebariFromFile(String f) {
        List<Intrebare> intrebari = new LinkedList<>();
        BufferedReader br = null;
        String line;
        List<String> intrebareAux;
        try {
            br = new BufferedReader(new FileReader(f));
            line = br.readLine();
            while (line != null) {
                intrebareAux = new LinkedList<>();
                while (!line.equals("##")) {
                    intrebareAux.add(line);
                    line = br.readLine();
                }
                String enunt = intrebareAux.get(0);
                InputValidation.validateEnunt(enunt);

                String varianta1 = intrebareAux.get(1);
                InputValidation.validateVarianta1(varianta1);

                String varianta2 = intrebareAux.get(2);
                InputValidation.validateVarianta2(varianta2);

                String variantaCorecta = intrebareAux.get(4);
                InputValidation.validateVariantaCorecta(variantaCorecta);

                String domeniu = intrebareAux.get(5);
                InputValidation.validateDomeniu(domeniu);

                Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, variantaCorecta, domeniu);
                intrebari.add(intrebare);
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        return intrebari;
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }
}