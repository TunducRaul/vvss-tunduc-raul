package evaluator.repository;

import evaluator.model.Intrebare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IntrebariRepoJpa extends JpaRepository<Intrebare, Integer> {
}