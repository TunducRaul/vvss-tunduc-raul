package evaluator.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "Intrebare")
@Table(name = "Intrebare")
public class Intrebare {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(100)")
    private Integer id;

    @Column(name = "ENUNT", nullable = false)
    private String enunt;

    @Column(name = "VARIANTA1", nullable = false)
    private String varianta1;

    @Column(name = "VARIANTA2", nullable = false)
    private String varianta2;

    @Column(name = "VARIANTA_CORECTA", nullable = false)
    private String variantaCorecta;

    @Column(name = "DOMENIU", nullable = false)
    private String domeniu;

    public Intrebare() {
        this.enunt = "";
        this.varianta1 = "";
        this.varianta2 = "";
        this.variantaCorecta = "";
        this.domeniu = "";
    }

    public Intrebare(String enunt, String varianta1, String varianta2, String variantaCorecta, String domeniu) {
        this.enunt = enunt;
        this.varianta1 = varianta1;
        this.varianta2 = varianta2;
        this.variantaCorecta = variantaCorecta;
        this.domeniu = domeniu;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnunt() {
        return enunt;
    }

    public void setEnunt(String enunt) {
        this.enunt = enunt;
    }

    public String getVarianta1() {
        return varianta1;
    }

    public void setVarianta1(String varianta1) {
        this.varianta1 = varianta1;
    }

    public String getVarianta2() {
        return varianta2;
    }

    public void setVarianta2(String varianta2) {
        this.varianta2 = varianta2;
    }

    public String getVariantaCorecta() {
        return variantaCorecta;
    }

    public void setVariantaCorecta(String variantaCorecta) {
        this.variantaCorecta = variantaCorecta;
    }

    public String getDomeniu() {
        return domeniu;
    }

    public void setDomeniu(String domeniu) {
        this.domeniu = domeniu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Intrebare intrebare = (Intrebare) o;
        return Objects.equals(enunt, intrebare.enunt) &&
                Objects.equals(varianta1, intrebare.varianta1) &&
                Objects.equals(varianta2, intrebare.varianta2) &&
                Objects.equals(variantaCorecta, intrebare.variantaCorecta) &&
                Objects.equals(domeniu, intrebare.domeniu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enunt, varianta1, varianta2, variantaCorecta, domeniu);
    }

    @Override
    public String toString() {
        return "Intrebare{" +
                "enunt='" + enunt + '\'' +
                ", varianta1='" + varianta1 + '\'' +
                ", varianta2='" + varianta2 + '\'' +
                ", variantaCorecta='" + variantaCorecta + '\'' +
                ", domeniu='" + domeniu + '\'' +
                '}';
    }
}