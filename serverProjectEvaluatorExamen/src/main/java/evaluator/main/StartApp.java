package evaluator.main;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Statistica;
import evaluator.model.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {
    private static final String file = "intrebari.txt";

    public static void main(String[] args) throws IOException {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        AppController appController = new AppController(file);
        boolean activ = true;
        String optiune = null;
        Scanner keyboard = new Scanner(System.in);

        while (activ) {
            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");
            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    try {
                        System.out.println("Dati enuntul intrebarii: ");
                        String enunt = keyboard.nextLine();
                        System.out.println("Dati prima varianta de raspuns: ");
                        String varianta1 = keyboard.nextLine();
                        System.out.println("Dati a doua varianta de raspuns: ");
                        String varianta2 = keyboard.nextLine();
                        System.out.println("Dati varianta corecta de raspuns: ");
                        String variantaCorecta = keyboard.nextLine();
                        System.out.println("Dati domeniul intrebarii: ");
                        String domeniu = keyboard.nextLine();
                        appController.addNewIntrebare(enunt, varianta1, varianta2, variantaCorecta, domeniu);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "2":
                    try {
                        Test test = appController.createNewTest();
                        System.out.println(test);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
                case "3":
                    try {
                        Statistica statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        // TODO
                        e.printStackTrace();
                    }
                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }
        keyboard.close();
    }
}