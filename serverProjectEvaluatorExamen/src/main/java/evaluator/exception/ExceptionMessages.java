package evaluator.exception;

public class ExceptionMessages {
    public static String ENUNT_VID = "Enuntul este vid!";
    public static String PRIMA_LITERA_ENUNT = "Prima litera din enunt nu e majuscula!";
    public static String PRIMA_VARIANTA_VIDA = "Varianta1 este vida!";
    public static String A_DOUA_VARIANTA_VIDA = "Varianta2 este vida!";
    public static String VARIANTA_INCORECTA = "Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}";
    public static String NU_EXISTA_SUFICIENTE_INTREBARI = "Nu exista suficiente intrebari pentru crearea unui test!(5)";
    public static String NU_EXISTA_SUFICIENTE_DOMENII = "Nu exista suficiente domenii pentru crearea unui test!(5)";
    public static String ULTIMUL_CARACTER_DIN_ENUNT = "Ultimul caracter din enunt nu e '?'!";
    public static String LUNGIME_ENUNT = "Lungimea enuntului depaseste 100 de caractere!";
    public static String INCEPUT_PRIMA_VARIANTA = "Varianta1 nu incepe cu '1)'!";
    public static String LUNGIME_PRIMA_VARIANTA = "Lungimea variantei1 depaseste 50 de caractere!";
    public static String INCEPUT_A_DOUA_VARIANTA = "Varianta2 nu incepe cu '2)'!";
    public static String LUNGIME_A_DOUA_VARIANTA = "Lungimea variantei2 depaseste 50 de caractere!";
    public static String DOMENIU_VID = "Domeniul este vid!";
    public static String PRIMA_LITERA_DOMENIU = "Prima litera din domeniu nu e majuscula!";
    public static String NU_EXISTA_INTREBARI = "Repository-ul nu contine nicio intrebare!";
    public static String INTREBARE_EXISTENTA = "Intrebarea deja exista!";
    public static String LUNGIME_DOMENIU = "Lungimea domeniului depaseste 30 de caractere!";
    public static String LUNGIME_A_TREIA_VARIANTA = "Lungimea variantei3 depaseste 50 de caractere!";
    public static String VARIANTA_TREI_VIDA = "Varianta3 este vida!";
    public static String INCEPUT_A_TRIA_VARIANTA = "Varianta3 nu incepe cu '3)'!";
}