package evaluator.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> handlerError(Exception exception, WebRequest request) {
        return new ResponseEntity<String>(
                exception.getMessage(),
                null,
                HttpStatus.BAD_GATEWAY
        );
    }
}