package evaluator.controller;

import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepoJpa;
import evaluator.util.InputValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestControllerServer {

    @Autowired
    public IntrebariRepoJpa repoJpa;

    @RequestMapping(value = "/intrebare", method = RequestMethod.POST, headers = {"content-type=application/json", "Access-Control-Allow-Origin=*"})
    @ResponseStatus(HttpStatus.OK)
    public Intrebare addIntrebare(@RequestBody Intrebare intrebare) throws Exception {
        System.out.println("Call accepted");
        Exception exception;
        try {
            InputValidation.validateEnunt(intrebare.getEnunt());
            InputValidation.validateVarianta1(intrebare.getVarianta1());
            InputValidation.validateVarianta2(intrebare.getVarianta2());
            InputValidation.validateVariantaCorecta(intrebare.getVariantaCorecta());
            InputValidation.validateDomeniu(intrebare.getDomeniu());
            return repoJpa.save(intrebare);
        } catch (Exception ex) {
            exception = ex;
            ex.printStackTrace();
        }
        throw exception;
    }
}