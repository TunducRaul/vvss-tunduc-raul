package evaluator.util;

import evaluator.exception.ExceptionMessages;
import evaluator.exception.InputValidationFailedException;

public class InputValidation {

    public static void validateEnunt(String enunt) throws InputValidationFailedException {
        enunt = enunt.trim();

        if (enunt.equals(""))
            throw new InputValidationFailedException(ExceptionMessages.ENUNT_VID);
        if (!Character.isUpperCase(enunt.charAt(0)))
            throw new InputValidationFailedException(ExceptionMessages.PRIMA_LITERA_ENUNT);
        if (!String.valueOf(enunt.charAt(enunt.length() - 1)).equals("?"))
            throw new InputValidationFailedException(ExceptionMessages.ULTIMUL_CARACTER_DIN_ENUNT);
        if (enunt.length() > 100)
            throw new InputValidationFailedException(ExceptionMessages.LUNGIME_ENUNT);
    }

    public static void validateVarianta1(String varianta1) throws InputValidationFailedException {
        varianta1 = varianta1.trim();

        if (varianta1.equals(""))
            throw new InputValidationFailedException(ExceptionMessages.PRIMA_VARIANTA_VIDA);
        if (!String.valueOf(varianta1.charAt(0)).equals("1") || !String.valueOf(varianta1.charAt(1)).equals(")"))
            throw new InputValidationFailedException(ExceptionMessages.INCEPUT_PRIMA_VARIANTA);
        if (varianta1.length() > 50)
            throw new InputValidationFailedException(ExceptionMessages.LUNGIME_PRIMA_VARIANTA);
    }

    public static void validateVarianta2(String varianta2) throws InputValidationFailedException {
        varianta2 = varianta2.trim();

        if (varianta2.equals(""))
            throw new InputValidationFailedException(ExceptionMessages.A_DOUA_VARIANTA_VIDA);
        if (!String.valueOf(varianta2.charAt(0)).equals("2") || !String.valueOf(varianta2.charAt(1)).equals(")"))
            throw new InputValidationFailedException(ExceptionMessages.INCEPUT_A_DOUA_VARIANTA);
        if (varianta2.length() > 50)
            throw new InputValidationFailedException(ExceptionMessages.LUNGIME_A_DOUA_VARIANTA);
    }

    public static void validateVarianta3(String varianta3) throws InputValidationFailedException {
        varianta3 = varianta3.trim();

        if (varianta3.equals(""))
            throw new InputValidationFailedException(ExceptionMessages.VARIANTA_TREI_VIDA);
        if (!String.valueOf(varianta3.charAt(0)).equals("3") || !String.valueOf(varianta3.charAt(1)).equals(")"))
            throw new InputValidationFailedException(ExceptionMessages.INCEPUT_A_TRIA_VARIANTA);
        if (varianta3.length() > 50)
            throw new InputValidationFailedException(ExceptionMessages.LUNGIME_A_TREIA_VARIANTA);

    }

    public static void validateVariantaCorecta(String variantaCorecta) throws InputValidationFailedException {
        variantaCorecta = variantaCorecta.trim();

        if (!variantaCorecta.equals("1") && !variantaCorecta.equals("2") && !variantaCorecta.equals("3"))
            throw new InputValidationFailedException(ExceptionMessages.VARIANTA_INCORECTA);
    }

    public static void validateDomeniu(String domeniu) throws InputValidationFailedException {
        domeniu = domeniu.trim();

        if (domeniu.equals(""))
            throw new InputValidationFailedException(ExceptionMessages.DOMENIU_VID);
        if (!Character.isUpperCase(domeniu.charAt(0)))
            throw new InputValidationFailedException(ExceptionMessages.PRIMA_LITERA_DOMENIU);
        if (domeniu.length() > 30)
            throw new InputValidationFailedException(ExceptionMessages.LUNGIME_DOMENIU);
    }
}