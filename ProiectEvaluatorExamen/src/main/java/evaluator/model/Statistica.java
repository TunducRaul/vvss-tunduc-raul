package evaluator.model;

import java.util.HashMap;
import java.util.Map;

public class Statistica {

    // String - Domeniu, Integer - Nr intrebari acel domeniu
    private Map<String, Integer> intrebariDomenii;

    public Statistica() {
        intrebariDomenii = new HashMap<>();
    }

    public void add(String key, Integer value) {
        intrebariDomenii.put(key, value);
    }

    public Map<String, Integer> getIntrebariDomenii() {
        return intrebariDomenii;
    }

    public void setIntrebariDomenii(Map<String, Integer> intrebariDomenii) {
        this.intrebariDomenii = intrebariDomenii;
    }

    public Integer getNumarIntrebari(String domeniu) {
        return this.intrebariDomenii.get(domeniu);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String domeniu : intrebariDomenii.keySet()) {
            sb.append(domeniu)
                    .append(": ")
                    .append(intrebariDomenii.get(domeniu))
                    .append("\n");
        }
        return sb.toString();
    }
}