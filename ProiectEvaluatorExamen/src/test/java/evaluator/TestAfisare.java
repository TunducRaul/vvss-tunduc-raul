package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.ExceptionMessages;
import evaluator.model.Statistica;
import org.junit.Test;

public class TestAfisare {
    private static String DOMENIU_VALID = "Intrebare1";

    @Test
    public void test_invalid_data() {
        try {
            AppController appController = new AppController("noData.txt");
            Statistica statistica = appController.getStatistica();
            assert false;
        } catch (Exception ex) {
            assert ex.getMessage().equals(ExceptionMessages.NU_EXISTA_INTREBARI);
        }
    }

    @Test
    public void test_valid_data() {
        try {
            AppController appController = new AppController("DateCorecte.txt");
            Statistica statistica = appController.getStatistica();
            assert statistica.getNumarIntrebari(DOMENIU_VALID).equals(1);
        } catch (Exception ex) {
            assert false;
        }
    }
}