package evaluator;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;

public class Test_BVA {

    private IntrebariRepository repo = new IntrebariRepository();

    @Test
    public void TC4_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("V?", "1) Foarte Multe", "2) Nici una", "1", "Matematica");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }

    @Test
    public void TC10_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate pere are Maria?", "1)Foarte Multe", "2) Nici una", "1", "Matematica");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }

    @Test
    public void TC16_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate pere are Maria?", "1) Foarte Multe", "2)Nici una", "1", "Matematica");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }

    @Test
    public void TC20_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate pere are Maria?", "1) Foarte Multe", "2) Nici una", "1", "Matematica");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }

    @Test
    public void TC28_BVA() throws DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cate pere are Maria?", "1) Foarte Multe", "2) Nici una", "1", "Z");
        String raspuns = repo.addIntrebare(intrebare);
        System.out.println(raspuns);
        assert raspuns.equals("quiz added");
    }
}